package ru.converter;

import javafx.application.Application;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.client.RestTemplate;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;

/**
 * Hello world!
 *
 */
@SpringBootApplication
public class App
{
    public static void main( String[] args ) throws JAXBException {
        SpringApplication.run(App.class, args);

        String url = "http://www.cbr.ru/scripts/XML_daily.asp";
        RestTemplate restTemplate = new RestTemplate();
        String answerServer = restTemplate.getForObject(url, String.class);
        answerServer = "<employee>" +
                "    <department>" +
                "        <id>101</id>" +
                "        <name>IT</name>" +
                "    </department>" +
                "    <firstName>Lokesh</firstName>" +
                "    <id>1</id>" +
                "    <lastName>Gupta</lastName>" +
                "</employee>";

        //try
        //{
            StringReader sr = new StringReader(answerServer);
            JAXBContext jaxbContext = JAXBContext.newInstance(Money.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            Money response = (Money) unmarshaller.unmarshal(sr);

            System.out.println(response);
        //}
        //catch (JAXBException e)
        //{
          //  e.printStackTrace();
        //}

    }
}
