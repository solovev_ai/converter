package ru.converter.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import ru.converter.dao.GetStat;
import ru.converter.dao.SingleCurs;
import ru.converter.service.CursService;
import java.util.List;


@RestController
@RequestMapping(value = "/cash")
public class Controller {

    private CursService cursService;

    @Autowired
    public void setCursService(CursService cursService) {
        this.cursService = cursService;
    }


    @GetMapping
    public ModelAndView calc(String message, String result) {
        String msg = message;
        ModelAndView modelAndView = new ModelAndView("fincalc");
        List<SingleCurs> cursList = cursService.findAll();
        modelAndView.addObject("cursList", cursList);
        modelAndView.addObject("msg", msg);
        modelAndView.addObject("result", result);
        return modelAndView;

    }

    @PostMapping(value = "/")
    public ModelAndView doCalc(String valute1, String valute2, String summ ) {
        String msg = "";
        String result = null;
        ModelAndView modelAndView = new ModelAndView("fincalc");
        modelAndView.addObject("msg", msg);
        try {
            result = cursService.getCalc(valute1, valute2, summ);
        } catch (IllegalArgumentException ex) {
            msg = "Не верный формат ввода! Укажите число.";
            return calc(msg, "0.0");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return calc(msg, result);
    }

    @GetMapping(value = "/stat")
    public ModelAndView getStat(int days) {
        ModelAndView modelAndView = new ModelAndView("stat");
        List<GetStat> statList = cursService.getStat(days);
        modelAndView.addObject("statList", statList);
        modelAndView.addObject("days", days);

        return modelAndView;
    }

    @PostMapping(value = "/stat")
    public ModelAndView getStatDays (int days) {
        ModelAndView modelAndView = new ModelAndView("stat");
        List<GetStat> statList = cursService.getStat(days);
        modelAndView.addObject("statList", statList);
        modelAndView.addObject("days", days);
        return getStat(days);
    }


    @GetMapping(value = "/all")
    public List<SingleCurs> findAll() {
      return cursService.findAll();
    }




}




