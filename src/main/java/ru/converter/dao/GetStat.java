package ru.converter.dao;

public class GetStat{
    private String valute1;
    private String valute2;
    private double summ;
    private long ddata;

    public GetStat() {
    }

    public GetStat(String valute1, String valute2, double summ, long ddata) {
        this.valute1 = valute1;
        this.valute2 = valute2;
        this.summ = summ;
        this.ddata = ddata;
    }

    public String getValute1() {
        return valute1;
    }

    public void setValute1(String valute1) {
        this.valute1 = valute1;
    }

    public String getValute2() {
        return valute2;
    }

    public void setValute2(String valute2) {
        this.valute2 = valute2;
    }

    public double getSumm() {
        return summ;
    }

    public void setSumm(double summ) {
        this.summ = summ;
    }

    public long getDdata() {
        return ddata;
    }

    public void setDdata(long ddata) {
        this.ddata = ddata;
    }

    @Override
    public String toString() {
        return "statistic{" +
                " valute1='" + valute1 + '\'' +
                ", valute2='" + valute2 + '\'' +
                ", Средний курс='" + summ + '\'' +
                ", Количество конвертаций=" + ddata +
                '}';
    }


}
