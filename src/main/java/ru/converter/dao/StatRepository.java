package ru.converter.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StatRepository extends CrudRepository<Statistic, Integer> {
    @Query("select s from Statistic s where s.ddata > ?1")
    List<Statistic> getStatData(int ddata);


    @Query("SELECT " +
        "    new ru.converter.dao.GetStat(s.valute1, s.valute2, avg(to_number(s.summ, '99999.99')) as summ, count(*) as ddata) " +
        "FROM " +
        "    Statistic s where s.ddata > ?1 " +
        "GROUP BY " +
        "    s.valute1, s.valute2")
    List<GetStat> getStat(int ddata);


}
