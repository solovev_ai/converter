package ru.converter.dao;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "statistic")
public class Statistic implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    @Column (name = "valute1")
    private String valute1;

    @Column (name = "valute2")
    private String valute2;

    @Column (name = "summ")
    private String summ;

    @Column (name = "ddata")
    private int ddata;

    public Statistic() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValute1() {
        return valute1;
    }

    public void setValute1(String valute1) {
        this.valute1 = valute1;
    }

    public String getValute2() {
        return valute2;
    }

    public void setValute2(String valute2) {
        this.valute2 = valute2;
    }

    public String getSumm() {
        return summ;
    }

    public void setSumm(String summ) {
        this.summ = summ;
    }

    public int getDdata() {
        return ddata;
    }

    public void setDdata(int ddata) {
        this.ddata = ddata;
    }

    @Override
    public String toString() {
        return "statistic{" +
                "id=" + id +
                ", valute1='" + valute1 + '\'' +
                ", valute2='" + valute2 + '\'' +
                ", summ='" + summ + '\'' +
                ", ddata=" + ddata +
                '}';
    }


}
