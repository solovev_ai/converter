package ru.converter.dao;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;

@XmlRootElement(name = "ValCurs")
@XmlAccessorType(XmlAccessType.FIELD)
public class ValCurs implements Serializable {

    @XmlAttribute(name = "Date")
    public String date;

    @XmlElement(name = "Valute")
    public ArrayList<SingleCurs> valutes;

    public ArrayList<SingleCurs> getValutes() {
        return valutes;
    }


    @Override
    public String toString() {
        return "ValCurs {" +
                "date='" + date + '\'' +
                ", valutes=" + valutes +
                " }";
    }

    public String getDate() {
        return date;
    }
}