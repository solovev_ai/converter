package ru.converter.service;

import org.springframework.web.client.RestTemplate;
import ru.converter.dao.ValCurs;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.Objects;

public class CursProvider {

    public CursProvider() {}
    // адрес, отдающий актуальный курс в формате XML
    String url = "http://www.cbr.ru/scripts/XML_daily.asp";

    public ValCurs GetCurs() throws IOException {
        // получаем ответ от сервера в виде строки
        RestTemplate restTemplate = new RestTemplate();
        String answerServer = restTemplate.getForObject(url, String.class);

//        String answerServer = "";
//        try {
//            BufferedReader br = new BufferedReader(new FileReader("D:\\JavaProjects\\Converter_git\\converter\\src\\main\\resources\\Test.xml"));
//            StringBuilder sb = new StringBuilder();
//            String line = br.readLine();
//
//            while (line != null) {
//                sb.append(line);
//                sb.append("\n");
//                line = br.readLine();
//            }
//            answerServer = sb.toString();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


        ValCurs valCurs = null;
        // при помощи библиотеки JAXB серрилизуем текствую информацию в объект java
        // под подготовленный pojo объект SingleCurs
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(ValCurs.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

            StringReader reader = new StringReader(Objects.requireNonNull(answerServer));
            valCurs = (ValCurs) unmarshaller.unmarshal(reader);

        } catch (JAXBException e) {
            e.printStackTrace();
        }
        // возвращаем объект
        return valCurs;
    }
}
