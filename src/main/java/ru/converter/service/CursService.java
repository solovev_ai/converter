package ru.converter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.converter.dao.*;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;


@Component
public class CursService {

    private CashRepository repository;

    @Autowired
    private void setRepository (CashRepository repository) {
        this.repository = repository;
    }

    @Autowired
    private StatRepository statRepository;

    @Autowired
    private void setStatRepository (StatRepository statRepository) {
        this.statRepository = statRepository;
    }

    private CursProvider cursProvider;
    private List<SingleCurs> valuteList;
    @Autowired
    public void setCursService(CursProvider cursProvider) {
        this.cursProvider = cursProvider;
    }


    @PostConstruct
    public void getValute() throws IOException {

        valuteList = cursProvider.GetCurs().valutes;

        // Обновляем дату курса у элементов
        valuteList.stream().forEach(valute -> {
            try {
                valute.setData(cursProvider.GetCurs().date);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        // полученные данные сохраняем в БД
        repository.saveAllAndFlush(valuteList);
    }


    public List<SingleCurs> findAll() {
        return repository.findAll();
    }

    public SingleCurs findeById(String id) {
        return repository.findById(id).orElse(new SingleCurs());

    }


    public String getCalc(String valute1, String valute2, String summ) throws IOException {

        if (!getActualCurs(valute1)) {
            getValute();
            System.out.println("Курс валют обновлен...");
        }

        SingleCurs curs1 = findeById(valute1);
        SingleCurs curs2 = findeById(valute2);

        double value1 = Double.parseDouble(curs1.getValue().replace(",", "."));
        int nominal1 = curs1.getNominal();
        double value2 = Double.parseDouble(curs2.getValue().replace(",", "."));
        int nominal2 = curs2.getNominal();
        double s = Double.parseDouble(summ.replace(",","."));

        // перевод из одной валюты в другую относительно рубля
        double res = ((value1 / nominal1 * s) / (value2 / nominal2));

        SingleCurs curs = findeById(valute1);
        String date = curs.getData();
        String year = date.substring(6);
        String month = date.substring(3,5);
        String dayOfMonth = date.substring(0, 2);

        int dateint = Integer.parseInt(year + month + dayOfMonth);

        Statistic stat = new Statistic();
        stat.setValute1(curs1.getName());
        stat.setValute2(curs2.getName());
        stat.setSumm("" + ( (value1 * nominal1) / (value2 * nominal2) ));
        stat.setDdata(dateint);

        statRepository.save(stat);

        return "" +  ((int)(res * 10000 + 0.5))/10000.0;

    }

    public boolean getActualCurs (String valute) {

        SingleCurs curs = findeById(valute);
        String date = curs.getData();
        String year = date.substring(6);
        String month = date.substring(3,5);
        String dayOfMonth = date.substring(0, 2);

        int dateint1 = Integer.parseInt(year + month + dayOfMonth);

        LocalDate dateNow = LocalDate.now(); // получаем текущую дату
        String year2 = "" + dateNow.getYear();
        String month2 = "" + dateNow.getMonthValue();
        String dayOfMonth2 = "" + dateNow.getDayOfMonth();

        if (month2.length()<10) {month2 = "0"+month2;}
        if (dayOfMonth2.length()<10) {dayOfMonth2 = "0"+dayOfMonth2;}

        int dateint2 = Integer.parseInt(year2 + month2 + dayOfMonth2);

        if (dateint1 < dateint2) {
            return false;
        }

        return true;
    }

    public List<GetStat> getStat(int days) {

        LocalDate dateNow = LocalDate.now(); // получаем текущую дату
        dateNow = dateNow.minusDays(days);
        String year = "" + dateNow.getYear();
        String month = "" + dateNow.getMonthValue();
        if (month.length() == 1) month = "0" + month;
        String dayOfMonth = "" + dateNow.getDayOfMonth();
        if (dayOfMonth.length() == 1) dayOfMonth = "0" + dayOfMonth;

        int dateint = Integer.parseInt(year + month + dayOfMonth) ;

        return statRepository.getStat(dateint);

    }



}
