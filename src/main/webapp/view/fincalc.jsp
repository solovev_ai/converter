<%@ page isELIgnored="false" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Конвертер валют</title>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
</head>
<body>
<main>
    <div>
        <h1>Конвертер валют</h1>
        <br>
         <h3 style="color: red">${msg}</h3>

        <form method = "POST"  action="" enctype="multipart/form-data">
        <select name="valute1" required>
                    <c:forEach items="${cursList}" var="cursList">
                        <option value="${cursList.getId()}">${cursList.getCharCode()} ${cursList.getValue()} (${cursList.getNominal()})</option>
                    </c:forEach>
          </select>

         &ensp; &ensp;
         <select name="valute2" required>
                     <c:forEach items="${cursList}" var="cursList">
                        <option value="${cursList.getId()}">${cursList.getCharCode()} ${cursList.getValue()} (${cursList.getNominal()})</option>
                      </c:forEach>
         </select><br>
         <br>
         <b>Сумма для конвертации:</b>
         <br>
         <input name="summ" value="1">
         <input type="hidden" name="days" readonly value=7>
         <br><br>
         <b>Результат:</b><br>
         <input name="result" type="text" readonly value="${result}">
         <br>
         <br>
         <!-- <input type="submit" value="Конвертировать"</input> -->
         <button type="submit"> Конвертировать </button>

        </form>

        <form action="stat" metod="POST">
                    <input type="hidden" name="days" readonly value=7>
                    <button type="submit">Просмотр статистики</button>
        </form>

        <!-- <p align="left"><a href="stat">Просмотр статистики</a> -->



    </div>
</main>


</body>
</html>