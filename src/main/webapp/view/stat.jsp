<%@ page isELIgnored="false" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Конвертер валют</title>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
</head>
<body>
<main>
    <div>
        <h1>Статистика по конвертациям валют</h1>
                <table border="1" width="600">
                        <tr>
                          <td>Валюта 1</td>
                          <td>Валюта 2</td>
                          <td>Средний курс</td>
                          <td>Кол-во конвертаций</td>
                        </tr>
                    <c:forEach items="${statList}" var="statList">
                        <tr>
                           <td>${statList.getValute1()}</td>
                           <td>${statList.getValute2()}</td>
                           <td>${statList.getSumm()}</td>
                           <td>${statList.getDdata()}</td>
                       </tr>
                    </c:forEach>

                </table>
    </div>

     <br>
     <form method="POST" action="stat" enctype="multipart/form-data">
         <b>Дней статистики: </b>
         <input name="days" value="${days}">
         <button type="submit">Получить</button>
         <button type="reset">Отмена</button>
     </form>

     <form>
             <input type="button" value="Назад" onclick="history.back()">
             <p align="left"><a href="/cash/">Домой</a>
     </form>


</main>


</body>
</html>